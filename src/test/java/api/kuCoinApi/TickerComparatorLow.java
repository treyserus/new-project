package api.kuCoinApi;

import java.util.Comparator;

public class TickerComparatorLow implements Comparator<TicketData> {
    @Override
    public int compare(TicketData o1, TicketData o2) {
        float result = Float.compare(Float.parseFloat(o1.getChangeRate()), Float.parseFloat(o2.getChangeRate()));
        return (int) result;
    }

}
