package api.kuCoinApi;

import api.Specifications;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.*;
import java.util.stream.Collectors;

import static io.restassured.RestAssured.given;

public class StreamApiExamplesTest {
    private final static String URL = "https://api.kucoin.com";

    public List<TicketData> getTickers() {
        Specifications.installSpecifications(Specifications.requestSpec(URL), Specifications.responseSpecOk());
        return given()
                .when()
                .get("/api/v1/market/allTickers")
                .then().log().all()
                .extract().jsonPath().getList("data.ticker", TicketData.class);
    }

    @Test
    public void checkCrypto() {
        List<TicketData> usdTickers = getTickers().stream().filter(x -> x.getSymbol().endsWith("USDT")).collect(Collectors.toList());
        Assert.assertTrue(usdTickers.stream().allMatch(x -> x.getSymbol().endsWith("USDT")));
    }

    @Test
    public void sortHighToLow() {
        List<TicketData> highToLow = getTickers().stream().filter(x -> x.getSymbol().endsWith("USDT")).sorted(new Comparator<TicketData>() {
            @Override
            public int compare(TicketData o1, TicketData o2) {
                return o2.getChangeRate().compareTo(o1.getChangeRate());
            }
        }).collect(Collectors.toList());
        List<TicketData> top10 = highToLow.stream().limit(10).collect(Collectors.toList());
        Assert.assertEquals(top10.get(0).getSymbol(), "STRAX-USDT");
    }

    @Test
    public void sortLowToHigh() {
        List<TicketData> lowToHigh = getTickers().stream().filter(x -> x.getSymbol().endsWith("USDT"))
                .sorted(new TickerComparatorLow()).limit(10).collect(Collectors.toList());
    }

    @Test
    public void map() {
        Map<String, Float> usd = new HashMap<>();
        getTickers().forEach(x -> usd.put(x.getSymbol(), Float.parseFloat(x.getChangeRate())));
        List<String> lowerCases = getTickers().stream().map(x -> x.getSymbol().toLowerCase()).collect(Collectors.toList());
        List<TickerShort> tickerShort = new ArrayList<>();
        getTickers().forEach(x -> tickerShort.add(new TickerShort(x.getSymbol(), Float.parseFloat(x.getChangeRate()))));
    }
}
