package api.cookies;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import io.restassured.http.ContentType;
import org.openqa.selenium.Cookie;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ui.selenide.BaseClass;


import java.util.Date;

import static com.codeborne.selenide.Selenide.$x;
import static io.restassured.RestAssured.given;

public class AuthUtil extends BaseClass {

    @BeforeClass
    public void init() {
        setUp();
    }

    @Test
    public void authWithRestCookiesTest() {
        Selenide.open("https://at-sandbox.workbench.lanit.ru/tickets/");
        String csrfToken = WebDriverRunner.getWebDriver().manage().getCookieNamed("csrftoken").getValue();

        String sessionId = given()
                .contentType(ContentType.MULTIPART)
                .cookies("csrftoken", csrfToken)
                .multiPart("username", "admin")
                .multiPart("password", "adminat")
                .multiPart("next", "/")
                .multiPart("csrfmiddlewaretoken", csrfToken)
                .post("https://at-sandbox.workbench.lanit.ru/login/")
                .then().log().all()
                .extract().cookie("sessionid");

        Date newDate = new Date();
        newDate.setTime(newDate.getTime() + (10000 * 10000));
        Cookie cookie = new Cookie("sessionid", sessionId, "at-sandbox.workbench.lanit.ru", "/", newDate);
        WebDriverRunner.getWebDriver().manage().addCookie(cookie);
        Selenide.refresh();
        $x("//a[@id='userDropdown']").should(Condition.text("admin"));
    }
}
