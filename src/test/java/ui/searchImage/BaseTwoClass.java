package ui.searchImage;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;

public class BaseTwoClass {
    public void setUp() {
//        WebDriverManager.chromedriver().setup();
        Configuration.browser = "chrome";
        Configuration.browserSize = "1920x1080";
        Configuration.headless = false;
        Configuration.pageLoadTimeout = 20000;
//        Configuration.pageLoadStrategy = "none";
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide());
    }

}
