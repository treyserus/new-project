package ui.searchImage;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.Keys;
import org.testng.ITestContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;

public class SearchTest extends BaseTwoClass {

    private final static String GOOGLE_URL = "https://www.google.ru";


    private final SelenideElement inputSearch = $x("//textarea[@id='APjFqb']");
    private final SelenideElement formSearch = $x("//form[@action='https://yandex.ru/search/']");
//    private final SelenideElement tabImage = $(byText("Картинки")).shouldBe(Condition.visible);
    private final SelenideElement tabImage = $x("//a[text() = 'Картинки']");
//    private final SelenideElement input = $("*#arrow__input.mini-suggest__input");

    @BeforeSuite
    public void repeatTests(ITestContext context) {
        Arrays.stream(context.getAllTestMethods()).forEach(x -> x.setRetryAnalyzerClass(TestNgRetry.class));
    }

    @BeforeClass
    public void init() {
        setUp();
    }

    @Test
    public void searchTest() {
        Selenide.open(GOOGLE_URL);
        WebDriverRunner.getWebDriver().manage().window().maximize();
        inputSearch.sendKeys("Сиамские котики" + Keys.ENTER);
        tabImage.shouldBe(visible, Duration.ofSeconds(10)).click();
        Selenide.sleep(6000);
        Selenide.executeJavaScript("window.scroll(0, 3000);");
        Selenide.sleep(2000);

        List<SelenideElement> links = $$x("//*[(text()='1600×1200')]/ancestor::a"); //Возможно нужно изменить xPath
        for (SelenideElement link : links) {
            String output = link.getAttribute("href");
            System.out.println(output);
        }

    }

//    @Test
//    public void urlTest() throws URISyntaxException, IOException, ParseException {
//        System.setProperty("webdriver.chrome.driver",
//                "C:\\driver\\chromedriver.exe");
//        ChromeDriver driver = new ChromeDriver();
//        driver.manage().window().maximize();
//        driver.get("https://yandex.ru");
//
//        String oldTab = driver.getWindowHandle();
//        driver.findElement(By.linkText("Картинки")).click();
//        ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
//        newTab.remove(oldTab);
//        driver.switchTo().window(newTab.get(0));
//        driver.findElement(By.name("text")).sendKeys("Сиамские котики" + Keys.ENTER);
//        try {
//            TimeUnit.SECONDS.sleep(2);
//        } catch (InterruptedException e) {
//            throw new RuntimeException(e);
//        }
//
//        JavascriptExecutor jse = (JavascriptExecutor) driver;
//        jse.executeScript("window.scrollBy(0,3000)", "");
//        try {
//            TimeUnit.SECONDS.sleep(2);
//        } catch (InterruptedException e) {
//            throw new RuntimeException(e);
//        }
// /*         jse.executeScript("window.scrollBy(0,3000)");
//        try {
//            TimeUnit.SECONDS.sleep(2);
//        } catch (InterruptedException e) {
//            throw new RuntimeException(e);
//        }*/
//        List<WebElement> links = driver.findElements(By.xpath("//*[(text()='1600×1200')]/../.."));
//        for (int i = 0; i < 5; i++) {
//            String link1 = links.get(i).getAttribute("href");
//            System.out.println(link1);
//
//        }
//    }

}
