package ui.selenide;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.Keys;

import static com.codeborne.selenide.Selenide.$x;

/**
 * Главная страница сайта Appleinsider.ru
 */
public class MainPage {
    private final SelenideElement checkBoxTrue = $x("//span[@class='mark']");
    private final SelenideElement textBoxInput = $x("//input[@type='text']");

    public MainPage(String url) {
        Selenide.open(url);
        WebDriverRunner.getWebDriver().manage().window().maximize();
    }

    /**
     * Выполняется поиск на сайте среди статей и нажимается кнопка Enter
     * @param searchString - поисковая строка
     */
    public SearchPage search(String searchString){
        Selenide.sleep(1000);
        textBoxInput.setValue(searchString);
        textBoxInput.sendKeys(Keys.ENTER);
        if (checkBoxTrue.isDisplayed()) {
            checkBoxTrue.click();
        }
        return new SearchPage();
    }
}
