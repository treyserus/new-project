package ui.selenide;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import io.github.bonigarcia.wdm.WebDriverManager;

public abstract class BaseClass {

    public void setUp() {
//        WebDriverManager.chromedriver().setup();
        Configuration.browser = "chrome";
        Configuration.browserSize = "1920x1080";
        Configuration.headless = false;
        Configuration.pageLoadTimeout = 10000;
        Configuration.pageLoadStrategy = "none";
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide());
    }

}