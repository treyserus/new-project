package ui.selenide;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.Description;
import io.qameta.allure.Owner;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import ui.searchImage.TestNgRetry;

import java.util.Arrays;

public class AppleTest extends BaseClass {

    private final static String BASE_URL = "https://appleinsider.ru/";
    private final static String SEARCH_STRING = "Чем iPhone 13 отличается от iPhone 12";
    private final static String EXPECTED_WORD = "iphone-13";

    @BeforeSuite
    public void repeatTests(ITestContext context) {
        Arrays.stream(context.getAllTestMethods()).forEach(x -> x.setRetryAnalyzerClass(TestNgRetry.class));
    }

    @BeforeClass
    public void init() {
        setUp();
    }

    @AfterClass
    public void tearDown() {
        Selenide.closeWebDriver();
    }

    @Test
    @Owner("Алексей Сафронов")
    @Description("Проверка работы поиска сайта")
    public void checkHref() {
//        MainPage mainPage = new MainPage(BASE_URL);
//        mainPage.search(SEARCH_STRING);
//        SearchPage searchPage = new SearchPage();
//        String href = searchPage.getHrefFromFirstArticle();
//        boolean contains = href.contains("iphone-13");
//        Assert.assertTrue(contains);
//todo    или
        Assert.assertTrue(new MainPage(BASE_URL)
                .search(SEARCH_STRING)
                .getHrefFromFirstArticle()
                .contains(EXPECTED_WORD));
    }

}
