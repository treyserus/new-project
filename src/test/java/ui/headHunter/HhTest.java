package ui.headHunter;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.Description;
import io.qameta.allure.Owner;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ui.selenide.BaseClass;

import java.util.HashMap;
import java.util.Map;

public class HhTest extends BaseClass {
    private static final String URL = "https://saransk.hh.ru/applicant/resumes/view?resume=dc4e0eb7ff0c4357b00039ed1f6c5349674c64";

    @BeforeClass
    public void init() {
        setUp();
    }

    @AfterClass
    public void tearDown() {
        Selenide.closeWebDriver();
    }

    @Test
    @Owner("Алексей Сафронов")
    @Description("Проверка некоторых данных в полях резюме, через карту")
    public void checkAttributesHashMap() {
        HhResumePage hhResumePage = new HhResumePage(URL);
        Map<String, Object> expectedAttributes = new HashMap<>();
        expectedAttributes.put(HhResumePage.GENDER, "М");
        expectedAttributes.put(HhResumePage.AGE, 29);
        expectedAttributes.put(HhResumePage.CITY, "Саранск");
        expectedAttributes.put(HhResumePage.PHONE_HIDDEN, true);
        expectedAttributes.put(HhResumePage.READY_TO_RELOCATE, false);

        Map<String, Object> actualAttributes = hhResumePage.getAttributes();

        Assert.assertEquals(expectedAttributes, actualAttributes);
    }

    @Test
    @Owner("Алексей Сафронов")
    @Description("Проверка некоторых данных в полях резюме, через класс")
    public void checkAttributesClass() {
        HhResumePage hhResumePage = new HhResumePage(URL);
        Resume expectedAttributes = new Resume("М", 29, "Саранск",
                true, false);
        Resume actualAttributes = new Resume(hhResumePage.getGender(), hhResumePage.getAge(), hhResumePage.getCity(),
                hhResumePage.isPhoneHidden(), hhResumePage.isReadyToRelocate());

        //Сравнение классов
        Assert.assertTrue(EqualsBuilder.reflectionEquals(expectedAttributes, actualAttributes));

        //Сравнение отдельных переменных
        Assert.assertEquals(expectedAttributes.getGender(), actualAttributes.getGender());
        Assert.assertEquals(expectedAttributes.getAge(), actualAttributes.getAge());
        Assert.assertEquals(expectedAttributes.getCity(), actualAttributes.getCity());
        Assert.assertEquals(expectedAttributes.isPhoneVisibility(), actualAttributes.isPhoneVisibility());
        Assert.assertEquals(expectedAttributes.isReadyToRelocate(), actualAttributes.isReadyToRelocate());
    }

}
