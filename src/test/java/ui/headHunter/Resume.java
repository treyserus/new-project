package ui.headHunter;

public class Resume {
    private String gender;
    private int age;
    private String city;
    private boolean isPhoneHidden;
    private boolean isReadyToRelocate;

    public Resume(String gender, int age, String city, boolean isPhoneHidden, boolean idReadyToRelocate) {
        this.gender = gender;
        this.age = age;
        this.city = city;
        this.isPhoneHidden = isPhoneHidden;
        this.isReadyToRelocate = idReadyToRelocate;
    }

    public String getGender() {
        return gender;
    }

    public int getAge() {
        return age;
    }

    public String getCity() {
        return city;
    }

    public boolean isPhoneVisibility() {
        return isPhoneHidden;
    }

    public boolean isReadyToRelocate() {
        return isReadyToRelocate;
    }
}
