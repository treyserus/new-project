package ui.headHunter;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import java.util.HashMap;
import java.util.Map;

import static com.codeborne.selenide.Selenide.$x;

public class HhResumePage {
    private final SelenideElement gender = $x("//span[@data-qa = 'resume-personal-gender']");
    private final SelenideElement age = $x("//span[@data-qa = 'resume-personal-age']/span");
    private final SelenideElement city = $x("//span[@data-qa = 'resume-personal-address']");
    private final SelenideElement liveData = $x("//span[@data-qa = 'resume-personal-address']/ancestor::p");
    private final SelenideElement hiddenPhoneNumber = $x("//div[@class='resume-hidden-field']");

    public static String GENDER = "Пол";
    public static String AGE = "Возраст";
    public static String CITY = "Город";
    public static String PHONE_HIDDEN = "Видимость телефона";
    public static String READY_TO_RELOCATE = "Готовность к переезду";


    public HhResumePage(String url) {
        Selenide.open(url);
    }

    public Map<String, Object> getAttributes() {
        return new HashMap<>(){{
            put(GENDER, getGender());
            put(AGE, getAge());
            put(CITY, getCity());
            put(PHONE_HIDDEN, isPhoneHidden());
            put(READY_TO_RELOCATE, isReadyToRelocate());
        }};
    }

    public boolean isPhoneHidden() {
        return hiddenPhoneNumber.isDisplayed();
    }

    public boolean isReadyToRelocate() {
        return !liveData.getText().split(", ")[1].equals("не готов к переезду");
    }

    public String getCity() {
        return city.getText();
    }

    public int getAge() {
        return Integer.parseInt(age.getText().replaceAll("\\D+", ""));
    }

    public String getGender() {
        return gender.getText().equals("Мужчина") ? "М" : "Ж";
     }

//    public String getGenderEasy() {
//        String genderValue = gender.getText();
//        if (genderValue.equals("Мужчина")) {
//            return "М";
//        }
//        return "Ж";
//    }

}
