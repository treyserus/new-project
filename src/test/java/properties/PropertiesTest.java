package properties;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

public class PropertiesTest {

    @Test
    public void readProperties() throws IOException {
        System.getProperties().load(ClassLoader.getSystemResourceAsStream("application.properties"));
        String urlFromProperties = System.getProperty("url");
        System.out.println(urlFromProperties);
    }

    @Test
    public void readFromConf() {
        String urlFromConf = ConfigProvider.URL;
        System.out.println(urlFromConf);
        Boolean isDemoAdmin = ConfigProvider.IS_DEMO_ADMIN;
        System.out.println(isDemoAdmin);
        Assert.assertFalse(isDemoAdmin);
        if (ConfigProvider.readConfig().getBoolean("usersParams.admin.isAdmin")) {
            System.out.println("Админ действительно админ");
        }
        else System.out.println("Админ не админ");
    }
}
