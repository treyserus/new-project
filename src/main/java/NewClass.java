import com.google.common.primitives.Ints;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class NewClass {
    public static void main(String[] arg) {
//     int[] arr = {3,43,56,22,3,4,7,6,77,3,45,21,23,54,33,4,22,45};
//
//     customSort(arr);
//
//     System.out.println(Arrays.toString(arr));

//      int number = 3;
//      int count = factorial(number);
//      System.out.println(count);

//     int[] nums1 = {1,2,3,0,0,0};
//     int[] nums2 = {2,5,6};
//     int m = 3;
//     int n = 3;
//     merge(nums1, m, nums2, n);

//     int[] nums = {2,3,3,2};
//     int val = 3;
//     int[] lenght = removeElement(nums, val);
//     System.out.println(Arrays.toString(lenght));

    int[] numss = {1,4,5,6,2,3,4,6,22,3,3,22,55,44,33,6,4,3};
    Integer[] ints = sortedDuplicatesTwo(numss);
    System.out.println(Arrays.toString(ints));

//    int[] numss1 = {1,3,3,6,1,3,1};
//    Arrays.sort(numss1);
//    int ints1 = removeDuplicatesOverTwo(numss1);
//    System.out.println(ints1);

//      int[] numss1 = {1,3,3,6,1,3,1,1,3,3};
//      int ints2 = majorityElement(numss1);
//      System.out.println(ints2);

//        String line = "one two three four two five one nine three one";
//        System.out.println(getDuplicatesThree(line));


    }

    public static int factorial(int n) {
        if (n <= 1)
            return 1;
        else
            return n * factorial(n - 1);
    }


    public static void customSort(int[] array) {
        int[] newArray = new int[100];
        for (int s : array) {
            newArray[s]++;
        }
        int m = 0;
        for (int i = 0; i < 100; i++) {
            while (newArray[i]-- > 0) {
                array[m++] = i;
            }
        }
    }

    /**
    Объединение отсортированного массива
     */
    public static void merge(int[] nums1, int m, int[] nums2, int n) {
        int[] intArr = Arrays.copyOf(nums1, m + n);
        System.arraycopy(nums2, 0, intArr, m, n);
        Arrays.sort(intArr);
        System.out.println(Arrays.toString(intArr));
    }

    /**
     Возвращает массив без указанных элементов "val"
     */
    public static int[] removeElement(int[] nums, int val) {
        List<Integer> list = new LinkedList<>();
        for (int item : nums) {
            if (val != item) {
                list.add(item);
            }
        }
        return Ints.toArray(list);
    }

    /**
     Возвращает длину массива отсортированного по уникальным значениям
     */
    public static int removeDuplicates(int[] nums) {
        int a = 0;
        int b = nums.length;
        int c = a + 1;
        while (a < b & c < b) {
            if (nums[a] == nums[c]) {
                c++;
            }
            else {
                nums[a + 1] = nums[c];
                a++;
            }
        }
        return a + 1;
    }

    public static int removeDuplicates2(int[] nums) {
        int index = 1;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i-1] != nums[i]) {
                nums[index] = nums[i];
                index++;
            }
        }
        return index;
    }

    /**
     Возвращает отсортированный массив по уникальным значениям
     */
    public static int[] sortedDuplicates(int[] nums) {
        Set<Integer> set = new TreeSet<>();
        for (int num : nums) {
            set.add(num);
        }
        return Ints.toArray(set);
    }

    public static Integer[] sortedDuplicatesTwo(int[] nums) {
        Set<Integer> set = new TreeSet<>();
        for (int num : nums) {
            set.add(num);
        }
        return set.toArray(new Integer[0]);
    }

    /**
     Возвращает длину массива отсортированного по уникальным значениям, до двух
     */
    public static int removeDuplicatesOverTwo(int[] nums) {
        Arrays.sort(nums);
        int j = 2;
        for (int i = 2; i < nums.length; i++) {
            if (nums[i] != nums[j-2]) {
                nums[j++] = nums[i];
            }
        }
        return j;
    }

    /**
     Возвращает самый часто повторяющийся элемент
     */
    public static int majorityElement(int[] nums) {
       int index = 0;
       int count = 0;
       for (int i = 1; i < nums.length; i++) {
           count = nums[index] == nums[i] ? count + 1 : count - 1;
           if (count < 0) {
               count = 0;
               index = i;
           }
       }
        return nums[index];
    }

    /**
     Сдвиг(поворот) элементов массива, на заданное число
     */
    public static void rotate(int[] nums, int k) {
        if (k > nums.length) {
            k = k % nums.length;
        }
        int[] result = new int[nums.length];
        int j = 0;
        for (int i = k; i < nums.length; i++){
            result[i] = nums[j];
            j++;
        }
        for (int i = 0; i < k; i++) {
            result[i] = nums[j];
            j++;
        }
        System.arraycopy(result, 0, nums, 0, nums.length);
    }

    static List<String> getDuplicates(String line) {
        String[] lines = line.split(" ");
        List<String> list = new LinkedList<>();
        int index = 0;
        int count = 1;
        int num = 1;
        for (int i = 1; i < lines.length; i++) {
            for (; num < lines.length; num++) {
                if (lines[index].equals(lines[num])) {
                    list.add(lines[num]);
                }
            }
            count++;
            index++;
            num = count;
        }
        return list;
    }

    static List<String> getDuplicatesTwo(String line) {
        String[] lines = line.split(" ");
        List<String> list = new LinkedList<>(Arrays.asList(lines));
        int index = 0;
        int count = 1;
        int num = 1;
        int a = 0;
        for (int i = 1; i < lines.length; i++) {
            for (; num < lines.length; num++) {
                if (lines[index].equals(lines[num])) {
                    a++;
                }
            }
            if (a == 0) {
                list.remove(lines[index]);
            }
            a = 0;
            count++;
            index++;
            num = count;
        }
        return list;
    }

    /**
     Ниже находятся методы, которые работают как нужно
     */

    /**
     Возвращает список, в котором только те элементы, кототорых при вхождении было больше 1.
     */
    static List<String> getDuplicatesThree(String line) {
        String[] lines = line.split(" ");
        Stream<String> stream = Stream.of(lines);
        Set<String> set = new LinkedHashSet<>();

        return stream.filter(n -> !set.add(n)).distinct()
                .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     Возвращает мапу, в которой указаны уникальные элементы и кол-во их повторений
     */
    static Map<String, Integer> getDuplicatesFour(String line) {
        String[] lines = line.split(" ");
        List<String> list = new LinkedList<>(Arrays.asList(lines));

        return list.stream()
                .collect(Collectors
                        .toMap(Function.identity(), value -> 1, Integer::sum));
    }

}
